package com.lim.sevaosa.magnets;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Lenovo on 07-01-2018.
 */

public class HelpActivity2 extends AppCompatActivity {
    private TextView txtView, txtView2, txtView3;
    private Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_help2);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        typeface = Typeface.createFromAsset(getAssets(), "fonts/lato-regular.ttf");

        txtView = findViewById(R.id.txtView);
        txtView2 = findViewById(R.id.txtView2);
        txtView3 = findViewById(R.id.txtView3);

        txtView.setTypeface(typeface);
        txtView2.setTypeface(typeface);
        txtView3.setTypeface(typeface);
    }
}
